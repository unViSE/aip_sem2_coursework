from src.libs.sort_algos import (shaker_sort, shell_sort,
                                 selection_sort, tim_sort)

from src.utils.theoretical_complexity import (quadratic_complexity,
                                              n_logarithmic_power_complexity,
                                              n_logarithm_n_complexity)

START = 10_000
END = 100_000
STEP = 10_000

SORTED_ALGORITHMS_NAMES = {
    '1': 'Двунаправленная пузырьковая сортировка',
    '2': 'Сортировка Шелла',
    '3': 'Сортировка выбором',
    '4': 'TimSort'}

SORTED_FUNCTION_DICT = {
    '1': shaker_sort,
    '2': shell_sort,
    '3': selection_sort,
    '4': tim_sort
}

LABELS = {
        'x': 'Длина списка',
        'y': ['Время(с)', 'Количество операций'],
        'title': SORTED_ALGORITHMS_NAMES
}

THEORETICAL_COMPLEXITY = {
    '1': quadratic_complexity,
    '2': n_logarithmic_power_complexity,
    '3': quadratic_complexity,
    '4': n_logarithm_n_complexity
}
