import pstats
import cProfile
from timeit import default_timer
from src.utils.useful_functions import remove_files_from_dir


def calls_counter(function, lines_to_print, *args, **kwargs):
    """Function for calculating the number of operations using
    cProfile and pstats

    @param function: class <function>
        Callable function
    @param lines_to_print: str
        The name of the desired function is entered in this parameter
    @return: int. Number of @param function calls.
    """
    filename = " .txt"
    with open(filename, 'w') as f:
        pr = cProfile.Profile()
        pr.enable()
        function(*args, **kwargs)
        pr.disable()
        p = pstats.Stats(pr, stream=f)
        p.strip_dirs().sort_stats().print_stats(lines_to_print)

    with open(filename) as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines]
        lines = [line for line in lines if line not in '']

    remove_files_from_dir('txt')
    return int(lines[4].split()[0])


def elapsed_time(function, *args, repeats=1, **kwargs):
    """Function for finding the amount of time spent on the
    @param function operation

    @param function: class <function>
        Callable function
    @param repeats: int, optional. Default is 1
        The number of repetitions of the desired function for
        a more accurate result
    @return: float. Time in s
    """
    result = 0
    for i in range(repeats):
        start_clock = default_timer()
        function(*args, **kwargs)
        end_clock = default_timer() - start_clock

        result += end_clock
    return result / repeats
