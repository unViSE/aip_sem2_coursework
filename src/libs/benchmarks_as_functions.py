from src.libs.benchmark_functions import (calls_counter, elapsed_time)
from src.utils.useful_functions import get_random_int_list
from src.const_values import SORTED_FUNCTION_DICT


def benchmark_calls(user_choice, interval):
    """Benchmark for finding the number of operations
     in a given interval

    @param user_choice: list[str,...]
        A list that consists of an unknown number
        of elements that are in the range from 1 to 4
    @param interval: list[int, int, int]
        Interval for the range function
        Ex: interval[start, end, step]
    @return: 2D list
    """
    data = []
    for i in user_choice:
        temp_data = []
        for j in range(interval[0], interval[1], interval[2]):
            if SORTED_FUNCTION_DICT[i] in [SORTED_FUNCTION_DICT['1'], SORTED_FUNCTION_DICT['3']]:
                temp_data.append(calls_counter(SORTED_FUNCTION_DICT[i], 'swap', get_random_int_list(j)))
            else:
                temp_data.append(calls_counter(SORTED_FUNCTION_DICT[i], 'replace', get_random_int_list(j)))
        data.append(temp_data)
        del temp_data
    return data


def benchmark_elapsed_time(user_choice, interval):
    """Benchmark for finding the time in SORTED_FUNCTION_DICT
    in a given interval

    @param user_choice: list[str,...]
        A list that consists of an unknown number
        of elements that are in the range from 1 to 4
    @param interval: list[int, int, int]
        Interval for the range function
        Ex: interval[start, end, step]
    @return: 2D list
    """
    data = []
    for i in user_choice:
        temp_data = []
        for j in range(interval[0], interval[1], interval[2]):
            temp_data.append(elapsed_time(SORTED_FUNCTION_DICT[i], get_random_int_list(j)))
        data.append(temp_data)
        del temp_data
    return data
