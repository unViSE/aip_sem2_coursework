def swap(array, first, second):
    """Function to swap the elements of one array"""
    array[first], array[second] = array[second], array[first]


def replace(array, first, second, choice=0):
    """Function to replace one element for another. If an invalid
    choice value is selected an exception(ValueError) will be raised
    """
    if choice == 0:
        array[first] = array[second]
    elif choice == 1:
        array[first] = second
    else:
        raise ValueError("Incorrect choice!")


def merge(arr, start, mid, end):
    if mid == end:
        return arr
    first = arr[start:mid + 1]
    last = arr[mid + 1:end + 1]
    len1 = mid - start + 1
    len2 = end - mid
    ind1 = 0
    ind2 = 0
    ind = start

    while ind1 < len1 and ind2 < len2:
        if first[ind1] < last[ind2]:
            arr[ind] = first[ind1]
            ind1 += 1
        else:
            arr[ind] = last[ind2]
            ind2 += 1
        ind += 1

    while ind1 < len1:
        arr[ind] = first[ind1]
        ind1 += 1
        ind += 1

    while ind2 < len2:
        arr[ind] = last[ind2]
        ind2 += 1
        ind += 1

    return arr


def find_min_run(n):
    minimum = 32
    r = 0
    while n >= minimum:
        r |= n & 1
        n >>= 1
    return n + r
