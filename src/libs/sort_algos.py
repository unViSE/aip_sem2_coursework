from copy import deepcopy
from src.libs.helper_functions import (swap,  replace,
                                       merge, find_min_run)


def shaker_sort(data, key=lambda x: x, reverse=False):
    """Sort the array in ascending order using bubble sorting method

    @param data: list
        A list filled with a random numbers
    @param key: class <function>, optional. Default is lambda x: x
        Key for sorting the array, need for 2D or more D array
    @param reverse: bool, optional
        Needed to return an inverted sorted array
    @return: list. The sorted list in ascending or descending order
    """
    if not data:
        return data

    array = deepcopy(data)

    left, right = 0, len(array) - 1

    while left <= right:
        for i in range(left, right, 1):
            if key(array[i]) > key(array[i + 1]):
                swap(array, i, i + 1)
        right -= 1

        for i in range(right, left, -1):
            if key(array[i - 1]) > key(array[i]):
                swap(array, i, i - 1)
        left += 1

    if not reverse:
        return array
    else:
        return array[::-1]


def shell_sort(data, key=lambda x: x, reverse=False):
    """Sort the array in ascending order using Shell sorting method

    @param data: list
        A list filled with a random numbers
    @param key: class <function>, optional. Default is lambda x: x
        Key for sorting the array, need for 2D or more D array
    @param reverse: bool, optional
        Needed to return an inverted sorted array
    @return: list. The sorted list in ascending or descending order
    """
    if not data:
        return data

    array = deepcopy(data)

    length = len(array)
    step = length // 2

    while step >= 1:

        for i in range(step, length):
            current = array[i]
            j = i
            while j > 0 and key(array[j - step]) > key(current):
                replace(array, j, j - step)
                j -= step
            replace(array, j, current, choice=1)
        step = step // 2

    if not reverse:
        return array
    else:
        return array[::-1]


def selection_sort(data, key=lambda x: x, reverse=False):
    """Sort the array in ascending order using selection sorting method

    @param data: list
        A list filled with a random numbers
    @param key: class <function>, optional. Default is lambda x: x
        Key for sorting the array, need for 2D or more D array
    @param reverse: bool, optional
        Needed to return an inverted sorted array
    @return: list. The sorted list in ascending or descending order
    """
    if not data:
        return data

    array = deepcopy(data)

    for i in range(len(array) - 1):
        for k in range(i+1, len(array)):
            if key(array[k]) < key(array[i]):
                swap(array, k, i)

    if not reverse:
        return array
    else:
        return array[::-1]



def insertion_sort(data, start, end, key=lambda x: x):
    """Additional sorting algorithm for tim_sort algo

    @param data: list
        A list filled with a random numbers
    @param start: int
    @param end: int
    @param key: class <function>, optional. Default is lambda x: x
    @return: sorted list
    """
    for index in range(start+1, end+1):
        current_value = data[index]
        current_position = index - 1
        while current_position >= start and key(current_value) < key(data[current_position]):
            replace(data, current_position + 1, current_position)
            current_position = current_position - 1
        replace(data, current_position + 1, current_value, choice=1)
    return data


def tim_sort(data, key=lambda x: x, reverse=False):
    """Sort the array in ascending order using insertion sort and merge
    function

    @param data: list
        A list filled with a random numbers
    @param key: class <function>, optional. Default is lambda x: x
        Key for sorting the array, need for 2D or more D array
    @param reverse: bool, optional
        Needed to return an inverted sorted array
    @return: list. The sorted list in ascending or descending order
    """
    if not data:
        return data

    array = deepcopy(data)

    n = len(array)
    min_run = find_min_run(n)

    for start in range(0, n, min_run):
        end = min(start+min_run-1, n-1)
        array = insertion_sort(array, start, end, key=key)

    current_size = min_run
    while current_size < n:
        for start in range(0, n, current_size*2):
            mid = min(n-1, start + current_size-1)
            end = min(n-1, mid + current_size)
            array = merge(array, start, mid, end)
        current_size *= 2

    if not reverse:
        return array
    else:
        return array[::-1]
