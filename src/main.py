from src.libs.benchmarks_as_functions import (benchmark_calls, benchmark_elapsed_time)
from src.utils.plot_functions import plot
from src.const_values import START, END, STEP


def main():
    print("Выберите нужные алгоритмы:\n",
          "1 - Двунаправленная пузырьковая сортировка\n",
          "2 - Сортировка Шелла\n",
          "3 - Сортировка выбором\n",
          "4 - Timsort\n")

    user_choice = str(input("Введите номера алгоритмов через пробел >> ")).split()

    try:
        data_time = benchmark_elapsed_time(user_choice, [START, END, STEP])
        data_calls = benchmark_calls(user_choice, [START, END, STEP])
    except KeyError:
        raise ValueError("Некорректный ввод!")

    # plot graphs to results DIR
    plot([i for i in range(START, END, STEP)], data_time, user_choice)
    plot([i for i in range(START, END, STEP)], data_calls, user_choice, y_label=1)
    plot([i for i in range(START, END, STEP)], data_calls, user_choice, y_label=1, theoretical=True)

    print("\nПрограмма завершила работу. Результаты программы сохранились в отдельные jpg файлы\n"
          "Результаты исследований можно найти в каталоге results по пути 'src/results'.")


if __name__ == "__main__":
    main()
