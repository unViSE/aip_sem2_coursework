import matplotlib.pyplot as plt
from src.const_values import LABELS, THEORETICAL_COMPLEXITY


def plot(size_of_array, data, user_choice, y_label=0, theoretical=False):
    """Function for displaying and saving graphs in DIR results

    @param size_of_array: list[int,...]
        List consisting of the numbers that were tested on
    @param data: 2D list
        List consisting of the results of sorting algorithms
    @param user_choice: list[str,...]
        A list that consists of an unknown number
        of elements that are in the range from 1 to 4
    @param y_label: int, optional. Default is 1
        Needed to select the name of the y-axis
        Ex: 1 - time(s)
            2 - Number of operations
    @param theoretical: boolean, optional. Default is False
        If enabled, the graph will show theoretical
        worst-case graphs of sorting algorithms
    @return: None
    """

    if len(data) == 1:
        fig, axes = plt.subplots(figsize=(15, 12))

        axes.plot(size_of_array, data[0], marker='o',
                  label=f"{LABELS['title'][user_choice[0]]} Средний случай(Практически)")

        axes.set(title=LABELS['title'][user_choice[0]], xlabel=f"{LABELS['x']}",
                 ylabel=f"{LABELS['y'][y_label]}")

        if theoretical:
            axes.plot(size_of_array, THEORETICAL_COMPLEXITY[user_choice[0]](size_of_array),
                      color='blue', label=f"{LABELS['title'][user_choice[0]]} Худший случай(Теоритически)")

        plt.grid(), plt.legend()

    else:
        fig, axes = plt.subplots(len(user_choice), figsize=(15, 20))

        for i in range(len(data)):
            axes[i].plot(size_of_array, data[i], marker='o',
                         label=f"{LABELS['title'][user_choice[i]]} Средний случай(Практически)")

            axes[i].set(title=f"{LABELS['title'][user_choice[i]]}",
                        xlabel=f"{LABELS['x']}", ylabel=f"{LABELS['y'][y_label]}")

            axes[i].grid(), axes[i].legend()

        if theoretical:
            for i in range(len(data)):
                axes[i].plot(size_of_array, THEORETICAL_COMPLEXITY[user_choice[i]](size_of_array),
                             color='blue', label=f"{LABELS['title'][user_choice[i]]} Худший случай(Теоритически)")
    if y_label == 0:
        plt.savefig('results/ResultGraphTime.jpg')

    elif y_label != 0 and theoretical is True:
        plt.savefig('results/ResultGraphCallsWithTheoretical.jpg')

    else:
        plt.savefig('results/ResultGraphCalls.jpg')