import os
from random import randint


def remove_files_from_dir(type_of_file=None):
    """Function for deleting all unnecessary files

    @param type_of_file: str, optional
        Type of deleting files
    @return: Void
    """
    path = os.getcwd()
    files = [f for f in os.listdir(path) if f.endswith(f".{type_of_file}")]
    for f in files:
        os.remove(os.path.join(path, f))


def get_random_int_list(size, lower=-64, upper=64):
    """

    @param size:
    @param lower:
    @param upper:
    @return:
    """
    return [randint(lower, upper) for i in range(size)]