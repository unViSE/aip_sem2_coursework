import pytest
from src.libs.sort_algos import (shaker_sort, shell_sort, selection_sort, tim_sort)


# CONST for tests
KEY = lambda x: x[1]
ARRAY = [(12, 3, 3), (4, 5, 6), (6, 5, 3), (83, 3, 3),
         (6, 8, 3), (83, 5, 2), (34, 3, 2), (6, 7, 9),
         (2, 1, 7), (5, 9, 3)]
SORTED_ARRAY = sorted(ARRAY)
SORTED_REVERSE_ARRAY = sorted(ARRAY, reverse=True)
SORTED_ARRAY_WITH_KEY = sorted(ARRAY, key=KEY)
SORTED_REVERSE_ARRAY_WITH_KEY = sorted(ARRAY, key=KEY, reverse=True)


def base_comparison(test_input, excepted, key):
    """Function for comparing a arrays
    Base comparison for all sort tests

    @param test_input: class
        What was received
    @param excepted: class
        What was excepted
    @param key:
    @return: None
    """
    for i in range(len(excepted)):
        if key(test_input[i]) != key(excepted[i]):
            assert False
    assert True


def test_correct_data():
    try:
        sorted(ARRAY, key=KEY)
    except IndexError:
        assert False
    else:
        assert True


# params for parametrized tests
ascending_sort_param = [(shaker_sort(ARRAY), SORTED_ARRAY),
                        (shell_sort(ARRAY), SORTED_ARRAY),
                        (selection_sort(ARRAY), SORTED_ARRAY),
                        (tim_sort(ARRAY), SORTED_ARRAY)]

descending_sort_param = [(shaker_sort(ARRAY, reverse=True), SORTED_REVERSE_ARRAY),
                         (shell_sort(ARRAY, reverse=True), SORTED_REVERSE_ARRAY),
                         (selection_sort(ARRAY, reverse=True), SORTED_REVERSE_ARRAY),
                         (tim_sort(ARRAY, reverse=True), SORTED_REVERSE_ARRAY)]


@pytest.mark.parametrize("test_input, excepted", ascending_sort_param)
def test_ascending_sort(test_input, excepted):
    base_comparison(test_input, excepted, KEY)


@pytest.mark.parametrize("test_input, excepted", descending_sort_param)
def test_descending_sort(test_input, excepted):
    base_comparison(test_input, excepted, KEY)


ascending_stable_sort_param = [(shaker_sort(ARRAY, key=KEY), SORTED_ARRAY_WITH_KEY),
                               (tim_sort(ARRAY, key=KEY), SORTED_ARRAY_WITH_KEY)]

descending_stable_sort_param = [(shaker_sort(ARRAY, key=KEY, reverse=True), SORTED_REVERSE_ARRAY_WITH_KEY),
                                (tim_sort(ARRAY, key=KEY, reverse=True), SORTED_REVERSE_ARRAY_WITH_KEY)]


@pytest.mark.parametrize("test_input, excepted", ascending_stable_sort_param)
def test_stable_ascending_sort(test_input, excepted):
    base_comparison(test_input, excepted, KEY)


@pytest.mark.parametrize("test_input, excepted", descending_stable_sort_param)
def test_stable_descending_sort(test_input, excepted):
    base_comparison(test_input, excepted, KEY)
